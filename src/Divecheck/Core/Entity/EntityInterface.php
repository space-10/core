<?php
namespace Divecheck\Core\Entity;

use Sirrus\Stdlib\ObjectInterface;
use Zend\Stdlib\ArraySerializableInterface;

/**
 * Short description for Sirrus\Entity$EntityInterface
 *
 * Long description for Sirrus\Entity$EntityInterface
 *
 * @copyright Copyright (c) 2014 Sirrus Systems GmbH (http://www.sirrus-systems.de/)
 * @license http://www.sirrus-systems.de/spf/license.html
 * @version $Id$
 * @since Class available since revision $Revision$
 */
interface EntityInterface extends ArraySerializableInterface
{

    /**
     * Returns the id of the entity.
     *
     * @return string int
     */
    public function getId();

    /**
     * Set the id of the entity.
     *
     * @param string|int $id
     * @return EntityInterface
     */
    public function setId($id);

}
