<?php
namespace Divecheck\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sirrus\Entity\AbstractORMEntity;

/**
 * Short description for Divecheck\Core\Entity$Website
 *
 * Long description for Divecheck\Core\Entity$Website
 *
 * @ORM\Entity
 * @ORM\Table(name="core_session")
 *
 * @copyright Copyright (c) 2014 Sirrus Systems GmbH (http://www.sirrus-systems.de/)
 * @version $Id$
 * @since Class available since revision $Revision$
 *
 */
class Session extends AbstractEntity
{

    /**
     * @ORM\Id
     * @ORM\Column(name="session_id", type="integer", nullable=false);
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Divecheck\Core\Entity\Website")
     * @ORM\JoinColumn(name="website_id", referencedColumnName="website_id")
     *
     * @var Website
     */
    protected $website;

    /**
     * @ORM\Column(name="session_expires", type="datetime", nullable=false)
     *
     * @var \DateTime
     */
    protected $expires;

    /**
     * @ORM\Column(name="session_data", type="blob", nullable=false)
     *
     * @var blob
     */
    protected $data;

    /**
     * (non-PHPdoc)
     *
     * @see \Sirrus\Entity\AbstractORMEntity::getId()
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Sirrus\Entity\AbstractORMEntity::setId()
     */
    public function setId($id)
    {

        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return \Divecheck\Core\Entity\Website
     */
    public function getWebsite()
    {

        return $this->website;
    }

    /**
     *
     * @param Website $website
     * @return \Divecheck\Core\Entity\Session
     */
    public function setWebsite(Website $website)
    {

        $this->website = $website;
        return $this;
    }

    /**
     *
     * @return DateTime
     */
    public function getExpires()
    {

        return $this->expires;
    }

    /**
     *
     * @param \DateTime $expires
     * @return \Divecheck\Core\Entity\Session
     */
    public function setExpires(\DateTime $expires)
    {

        $this->expires = $expires;
        return $this;
    }

    /**
     * Returns true or false if this session is expired or not.
     *
     * If $expireDate is null it will be replaced with the current time.
     *
     * @param \DateTime $expiredDate
     *            DateTime object to check against if the session is still valid at that time
     * @return boolean True if $expiredDate is greater that the expires date, false otherwise.
     */
    public function isExpired(\DateTime $expiredDate = null)
    {

        if ($expiredDate === null)
        {
            $expiredDate = new \DateTime('now');
        }

        return $expiredDate <= $this->expires;
    }

    /**
     *
     * @return \Divecheck\Core\Entity\blob
     */
    public function getData()
    {

        return $this->data;
    }

    /**
     *
     * @param mixed $data
     * @return \Divecheck\Core\Entity\Session
     */
    public function setData($data)
    {

        $this->data = $data;
        return $this;
    }
}
