<?php
namespace Divecheck\Core\Entity;

use Magento\Framework\Stdlib\StringUtils;

/**
 * Class AbstractEntity
 * @package Divecheck\Core\Entity
 */
abstract class AbstractEntity implements EntityInterface
{
    /**
     * Setter/Getter underscore transformation cache
     *
     * @var array
     */
    private static $underscoreCache = [];

    /**
     * @param EntityInterface $o
     *
     * @return bool
     */
    public function equals(EntityInterface $o)
    {

        if (!$o instanceof EntityInterface) {
            return false;
        }

        return $this->getId() === $o->getId();
    }

    /**
     * Returns the id of the entity.
     *
     * @return string int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the id of the entity.
     *
     * @param string|int $id
     *
     * @return EntityInterface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     *
     * @return void
     */
    public function exchangeArray(array $array)
    {

        foreach ($array as $key => $value) {
            $property = StringUtils::upperCaseWords($key, '_', '');;
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $vars = get_object_vars($this);
        $arr = [];
        foreach ($vars as $key => $value) {
            $arr[$this->underscore($key)] = $value;
        }
        return $arr;
    }

    /**
     * Converts field names for setters and getters
     *
     * $this->setMyField($value) === $this->setData('my_field', $value)
     * Uses cache to eliminate unnecessary preg_replace
     *
     * @param string $name
     *
     * @return string
     */
    protected function underscore($name)
    {

        if (isset(static::$underscoreCache[$name])) {
            return static::$underscoreCache[$name];
        }
        $result = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $name));
        static::$underscoreCache[$name] = $result;
        return $result;
    }
}
