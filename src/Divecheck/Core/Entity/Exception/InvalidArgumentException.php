<?php
namespace Divecheck\Core\Entity\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
