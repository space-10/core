<?php
namespace Divecheck\Core\Entity\Exception;

class StoreException extends \InvalidArgumentException implements ExceptionInterface
{
}
