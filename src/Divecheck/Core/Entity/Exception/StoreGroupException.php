<?php
namespace Divecheck\Core\Entity\Exception;

class StoreGroupException extends \InvalidArgumentException implements ExceptionInterface
{
}
