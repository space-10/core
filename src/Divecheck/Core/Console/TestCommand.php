<?php
namespace Divecheck\Core\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    protected function configure()
    {
        $this
        ->setName('divecheck:store')
        ->setDescription('A Test.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

    }
}
