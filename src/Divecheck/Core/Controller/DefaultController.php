<?php
namespace Divecheck\Core\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Util\Debug;
use Doctrine\DBAL\Schema\View;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class DefaultController
 * @package Divecheck\Core\Controller
 */
class DefaultController extends AbstractActionController implements ObjectManagerAwareInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    public function indexAction()
    {
        $repo = $this->getObjectManager()->getRepository('Space10\SystemSettings\Entity\Config');
        $configEntries = $repo->findAll();
        // Debug::dump($configEntries);
        /* @var $em \Doctrine\ORM\EntityManager */
//        $locator = $this->getServiceLocator();
//        $s1 = $locator->get('StoreManager');
//        $s1->init();
//        $websites = $s1->getWebsites();
//        $websites = $repo->findAll();
//        foreach ($websites as $entityAttribute) {
//            $entityType = $entityAttribute->getEntityType();
//            $attributeSet = $entityAttribute->getAttributeSet();
//            $attributeGroup = $entityAttribute->getAttributeGroup();
//            $attribute = $entityAttribute->getAttribute();

//             Debug::dump($entityType->getEntityTypeName());
//             Debug::dump($attributeSet->getAttributeGroups());
//             Debug::dump($attributeGroup->getAttributeGroupName());
//             Debug::dump($attribute->getAttributeCode());
//             Debug::dump($entityAttribute);
//        }
        $model = new ViewModel();
        $model->setTemplate('index');

        return $model;
    }

    /**
     * Set the object manager
     *
     * @param ObjectManager $objectManager
     */
    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Get the object manager
     *
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }
}
