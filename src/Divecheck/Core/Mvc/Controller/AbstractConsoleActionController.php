<?php
namespace Divecheck\Core\Mvc\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Console\Request as ConsoleRequest;
use Zend\Stdlib\RequestInterface;
use Zend\Stdlib\ResponseInterface;

class AbstractConsoleActionController extends AbstractActionController
{

    public function dispatch(RequestInterface $request, ResponseInterface $response = null)
    {

        if (! $request instanceof ConsoleRequest)
        {
            throw new Exception\InvalidArgumentException('Expected an console request');
        }
        
        return parent::dispatch($request, $response);
    }
}
