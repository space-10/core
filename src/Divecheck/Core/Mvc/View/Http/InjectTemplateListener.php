<?php
namespace Divecheck\Core\Mvc\View\Http;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\View\Http\InjectTemplateListener as ZendInjectTemplateListener;
use Zend\View\Model\ModelInterface as ViewModel;
use Zend\Mvc\ModuleRouteListener;

/**
 * Class InjectTemplateListener
 * @package Divecheck\Core\Mvc\View\Http
 */
class InjectTemplateListener extends ZendInjectTemplateListener
{

    /**
     * @param EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events)
    {
        $sharedEvents = $events->getSharedManager();
        $this->listeners[] = $sharedEvents->attach('Zend\Stdlib\DispatchableInterface', MvcEvent::EVENT_DISPATCH, [$this, 'injectTemplate']);
    }

    /**
     * Inject a template into the view model, if none present
     *
     * Template is derived from the controller found in the route match, and,
     * optionally, the action, if present.
     *
     * @param  MvcEvent $e
     * @return void
     */
    public function injectTemplate(MvcEvent $e)
    {

        $model = $e->getResult();
        if (! $model instanceof ViewModel)
        {
            return;
        }
        
        $template = $model->getTemplate();
        if (! empty($template))
        {
            return;
        }
        
        $routeMatch = $e->getRouteMatch();
        $controller = $e->getTarget();
        if (is_object($controller))
        {
            $controller = get_class($controller);
        }
        
        if (! $controller)
        {
            $controller = $routeMatch->getParam('controller', '');
        }
        
        $module = $this->deriveModuleNamespace($controller);
        if ($namespace = $routeMatch->getParam(ModuleRouteListener::MODULE_NAMESPACE))
        {
            $controllerSubNs = $this->deriveControllerSubNamespace($namespace);
            if (! empty($controllerSubNs))
            {
                if (! empty($module))
                {
                    $module .= '/' . $controllerSubNs;
                }
                else
                {
                    $module = $controllerSubNs;
                }
            }
        }
        
        $controller = $this->deriveControllerClass($controller);
        $template = $this->inflectName($module);
        // $template = strtolower($module);
        
        if (! empty($template))
        {
            $template .= '/';
        }
        $template .= $this->inflectName($controller);
        // $template .= strtolower($controller);
        
        $action = $routeMatch->getParam('action');
        if (null !== $action)
        {
            $template .= '/' . $this->inflectName($action);
        }
        $model->setTemplate($template);
    }

    /**
     * Determine the top-level namespace of the controller
     *
     * @param  string $controller
     * @return string
     */
    protected function deriveModuleNamespace($controller)
    {

        if (! strstr($controller, '\\'))
        {
            return '';
        }
        
        $parts = explode('\\', $controller);
        $vendorModuleNS = [
            array_shift($parts),
            array_shift($parts)
        ];
        $ns = implode('\\', $vendorModuleNS);
        
        if (! class_exists($ns . '\Module', true))
        {
            return parent::deriveModuleNamespace($controller);
        }
        
        return implode('/', $vendorModuleNS);
    }
}
