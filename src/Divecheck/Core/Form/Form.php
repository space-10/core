<?php
/**
 * Created by PhpStorm.
 * User: Tobias Trozowski
 * Date: 19.09.2015
 * Time: 13:08
 */

namespace Divecheck\Core\Form;

use Zend\Form\Element;

/**
 * Class Form
 * @package Divecheck\Core\Form
 */
class Form extends \Zend\Form\Form
{
    public function init()
    {
        $this->setAttribute('class', 'form');
        $this->setAttribute('role', 'form');

        $this->add(new Element\Csrf('csrf'));
    }
}