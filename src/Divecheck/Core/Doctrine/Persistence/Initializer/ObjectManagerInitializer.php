<?php
namespace Divecheck\Core\Doctrine\Persistence\Initializer;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\Form\FormElementManager;
use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\InitializerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ObjectManagerInitializer
 * @package Divecheck\Core\Doctrine\Persistence\Initializer
 */
class ObjectManagerInitializer implements InitializerInterface
{
    /**
     * Initialize
     *
     * @param                         $instance
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        if ($serviceLocator instanceof ControllerManager || $serviceLocator instanceof FormElementManager) {
            $serviceLocator = $serviceLocator->getServiceLocator();
        }

        if ($instance instanceof ObjectManagerAwareInterface) {
            /**
             * @var $em EntityManager
             */
            $em = $serviceLocator->get(EntityManager::class);
            $instance->setObjectManager($em);
        }
    }
}
