<?php
namespace Divecheck\Core\DoctrineModule\Service;
use DoctrineModule\Service\CliFactory as DoctrineCliFactory;
use Zend\ServiceManager\ServiceLocatorInterface;
use DoctrineModule\Version;
use Symfony\Component\Console\Application;

class CliFactory extends DoctrineCliFactory
{

    /**
     * (non-PHPdoc)
     * @see \DoctrineModule\Service\CliFactory::createService()
     */
    public function createService(ServiceLocatorInterface $sl)
    {
        // return empty application to prevent the loadCli.post event (we dont need it in non-cli mode)
        if (PHP_SAPI !== 'cli') {
            $cli = new Application;
            return $cli;
        }
        return parent::createService($sl);
    }
}
