<?php
namespace Divecheck\Core\Stdlib\Initializer;

use Zend\ServiceManager\InitializerInterface;
use Zend\Stdlib\InitializableInterface;

class InitializableInitializer implements InitializerInterface
{
    /*
     * (non-PHPdoc) @see \Zend\ServiceManager\InitializerInterface::initialize()
     */
    public function initialize($instance,\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        // dont init when element or input filter is used
        // they will be initialized seperated
        if ($instance instanceof InitializableInterface && !$instance instanceof \Zend\Form\Element && !$instance instanceof \Zend\InputFilter\BaseInputFilter)
        {
            $instance->init();
        }
    }
}
