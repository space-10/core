<?php
namespace Divecheck\Core;

use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Divecheck\Core\Mvc\View\Http\InjectTemplateListener;

/**
 * Short description for Divecheck\Core$Module
 *
 * Long description for Divecheck\Core$Module
 *
 * @author Tobias Trozowski
 * @copyright Copyright (c) 2013 Sirrus Systems GmbH (http://www.sirrus-systems.de/)
 * @version $Id$
 * @since $Revision$
 * @link http://framework.zend.com/manual/2.0/en/modules/zend.console.introduction.html
 */
class Module implements ConfigProviderInterface, BootstrapListenerInterface, DependencyIndicatorInterface
{

    /*
     * (non-PHPdoc) @see \Zend\ModuleManager\Feature\DependencyIndicatorInterface::getModuleDependencies()
     */
    public function getModuleDependencies()
    {

        return [
            'DoctrineModule',
            'DoctrineORMModule'
        ];
    }

    /*
     * (non-PHPdoc) @see \Zend\ModuleManager\Feature\ConfigProviderInterface::getConfig()
     */
    public function getConfig()
    {

        return include __DIR__ . '/../../../etc/module.config.php';
    }

    public function onBootstrap(EventInterface $event)
    {

        /* @var $app \Zend\Mvc\Application */
        $app = $event->getApplication();
        $serviceManager = $app->getServiceManager();

        $cli = $serviceManager->get('doctrine.cli');
        $config = $serviceManager->get('Config');

//        $translator
//            ->setLocale(\Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']))
//            ->setFallbackLocale('en_US');

        $cli->addCommands(array_map([
            $serviceManager,
            'get'
        ], $config['console']['commands']));
    }
}