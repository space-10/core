<?php
namespace Divecheck\Core;

use Divecheck\Core\Console\TestCommand;
use Divecheck\Core\Controller\DefaultController;
use Divecheck\Core\Doctrine\Persistence\Initializer\ObjectManagerInitializer;
use Divecheck\Core\DoctrineModule\Service\CliFactory;
use Divecheck\Core\Mvc\View\Http\InjectTemplateListener;
use Divecheck\Core\Stdlib\Initializer\InitializableInitializer;
use Divecheck\Core\View\Helper\FormElementManager;
use Zend\I18n\Translator\TranslatorAwareInterface;
use Zend\Mvc\Controller\PluginManager;
use Zend\ServiceManager\ServiceLocatorInterface;

return [
    'space10'            => [
        'di' => [
            'config_paths' => [
                'etc/di.xml',
                '../../../etc/di.xml',
            ],
        ],
    ],
    'space10admin'       => [
        'config_paths' => [
            'etc/admin/menu.xml',
            '../../../etc/admin/menu.xml',
        ],
    ],
    'controllers'        => [
        'invokables'   => [
            DefaultController::class => DefaultController::class,
        ],
        'aliases'      => [
            'Divecheck\Core\Default' => DefaultController::class,
        ],
        'initializers' => [
            ObjectManagerInitializer::class => ObjectManagerInitializer::class,
            InitializableInitializer::class => InitializableInitializer::class,
        ],
    ],
    'controller_plugins' => [
        'invokables'   => [
            #Plugin\Translate::class => Plugin\Translate::class,
            #Plugin\TranslatePlural::class => Plugin\TranslatePlural::class,
        ],
        'initializers' => [
            TranslatorAwareInterface::class => function ($instance, ServiceLocatorInterface $serviceLocator) {
                if ($serviceLocator instanceof PluginManager) {
                    $serviceLocator = $serviceLocator->getController()->getServiceLocator();
                }

                if ($instance instanceof TranslatorAwareInterface) {
                    $instance->setTranslator($serviceLocator->get('translator'));
                }
            },
        ],
        'aliases'      => [
            #'translate' => Plugin\Translate::class,
            #'translatePlural' => Plugin\TranslatePlural::class,
        ],
    ],
    'form_elements'      => [
        'initializers' => [
            ObjectManagerInitializer::class => ObjectManagerInitializer::class,
        ],
    ],
    'event_manager'      => [
        'listener' => [
            InjectTemplateListener::class,
        ],
    ],
    'view_helpers'       => [
        'invokables' => [
            FormElementManager::class => FormElementManager::class,
        ],
        'aliases'    => [
            'formelementmanager' => FormElementManager::class,
        ],
    ],
    'service_manager'    => [
        'invokables'         => [
            InjectTemplateListener::class => InjectTemplateListener::class,
        ],
        'initializers'       => [
            // initializer to inject the divecheck core config (from database.core_config_data)
            // ObjectManagerInitializer::class => ObjectManagerInitializer::class,
//            InitializableInitializer::class => InitializableInitializer::class,
        ],
        'factories'          => [
            'doctrine.cli' => CliFactory::class,
        ],
        'abstract_factories' => [
            // 'Sirrus\Di\Service\AbstractServiceFactory' => 'Sirrus\Di\Service\AbstractServiceFactory'
        ],
    ],
    'router'             => [
        'routes' => [
            'home' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => DefaultController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
            ],
        ],
    ],
    'console'            => [
        'commands' => [
            TestCommand::class,
        ],
    ],
    'translator'         => [
        'locale'                    => 'en_US',
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ],
        ],
    ],
    'view_manager'       => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'template_path_stack'      => [
            /* 'core' => */
            __NAMESPACE__ => __DIR__ . '/../view',
        ],
    ],
];
